import React from 'react'
import { NavigationContainer } from "@react-navigation/native"
import AppStack from "./src/Route/AppStack"
import { Provider } from 'react-redux'
import storeRedux from './src/redux/store'
// import storeRedux from './src/Redux/Store';
const App = () => {
  return (
    <Provider store={storeRedux}>
      <NavigationContainer>
        <AppStack />
      </NavigationContainer>
    </Provider>
  )
}

export default App