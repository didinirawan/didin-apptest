import React from 'react';
import {
    createStackNavigator,
    CardStyleInterpolators,
  } from '@react-navigation/stack';
import EditContact from '../Screens/EditContact';
import Index from '../Screens/Index';
import AddContact from '../Screens/AddContact';
const Stack = createStackNavigator();

const AppStack = () => {
    return (
        <>
            <Stack.Navigator screenOptions={{headerShown: false, cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>
                <Stack.Screen
                    name='Index'
                    component={Index}
                />
                <Stack.Screen
                    name='EditContact'
                    component={EditContact}
                />
                <Stack.Screen
                    name='AddContact'
                    component={AddContact}
                />
                
            </Stack.Navigator>
        </>
    )
}

export default AppStack;