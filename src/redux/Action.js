import axios from "axios"
import { Alert } from "react-native";

const baseUrl = 'https://simple-contact-crud.herokuapp.com/contact'
// export const GetContact = () => {
//     try {
//         return async (dispatch, getState) => {
//             var config = {
//                 method: 'get',
//                 url: baseUrl,
//                 headers: {
//                     'Accept': 'application/json',
//                 }
//             }
//             await axios(config)
//              .then(function (response) {
//                     dispatch({
//                         type: 'GET_CONTACT_SUCCESS',
//                         data: response.data.data,
//                     })
//                     return response;
//             })
//             .catch(function (err){
//                 console.log('error', err);
//             })
//             return true
//         }
//     } catch (err) {
//         console.log("error GetContact", err);
//     }
// }
export const GetContact =()=>{
    try{
       return async (dispatch,getState)=>{ 
        var config = {
            method: 'get',
            url: 'https://simple-contact-crud.herokuapp.com/contact',
            headers: {
                'Accept': 'application/json',
            },
            // data: data,
          };

      const ini =await axios(config)
        .then(function (response) {
        //   console.log('resdata1', response.data);
          dispatch({
            type: 'GET_CONTACT_SUCCESS',
            data: response.data.data,
          });
          return response;
        })
        .catch(function (error) {
          console.log('erro', error);
        });
        // console.log('iniss',ini.data.data);
      return true;
    }
    }catch(e){
        console.log('ini error',e);
    }
    
}
export const GetContactById =(payload)=>{
    try{
       return async (dispatch,getState)=>{ 
        var config = {
            method: 'get',
            url: 'https://simple-contact-crud.herokuapp.com/contact/'+payload+'',
            headers: {
                'Accept': 'application/json',
            },
            // data: data,
          };

      const ini =await axios(config)
        .then(function (response) {
          console.log('resdata1', response.data);
          dispatch({
            type: 'GET_CONTACT_BY_ID',
            data: response.data.data,
            isLoading: false
          });
          return response;
        })
        .catch(function (error) {
          console.log('erro', error);
        });
        // console.log('iniss',ini.data.data);
      return true;
    }
    }catch(e){
        console.log('ini error',e);
    }
    
}
export const editContact = (payload, id) => {
    console.log('ini payload',payload);
    console.log('ini payload id',id);
    return async (dispatch, getState) => {
        try{
            var config = {
                method: 'put',
                url: `https://simple-contact-crud.herokuapp.com/contact/${id}`,
                headers: {
                'Accept': 'application/json',
                "content-type": "application/json; charset=utf-8"
                },
                data: payload,
            };
            
            await axios(config)
            .then(function (response) {
                console.log('resdata2', response.config.data);
                Alert.alert("Sukses edit Contact")
                return response;
            })
            .catch(function (error) {
                console.log('erro', error);
            });
            return true;  

        }catch(e){

        }
    }
}

export const addContact=(payload)=>{
    console.log('ini payload add ',payload);
    return async (dispatch, getState) => {
    try{
        var config = {
            method: 'post',
            url: `https://simple-contact-crud.herokuapp.com/contact`,
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            data: payload,
          };
        
        await axios(config)
        .then(function (response) {
            console.log('resdata3', response.config.data);
            Alert.alert("Sukses add Contact")
            return response;
          })
          .catch(function (error) {
            console.log('erro', error);
          });
        return true;  

    }catch(e){

    }
}
}

export const DeleteContact = (payload) => {
    console.log(payload,'ini payload delete');
    // axios.delete
    return async (dispatch, getState) => {
        try {
            await axios.delete(
                `${baseUrl}/${payload}`,
                {
                    headers: {
                        'Accept': 'application/json',
                    },
                },
            );
            return true;
        } catch (err) {
            console.log(err, 'error delete');
        }
    }
}