import {combineReducers} from 'redux';
import fetch from './reducer';

export default combineReducers({
  fetch,
});