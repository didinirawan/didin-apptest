import { SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { responsiveHeight } from '../Utils/ResponsiveUI'
import CardListContact from '../Component/Card/CardListContact'
import Header from '../Component/Headers/Header'
import ModalDelete from '../Component/Modal/ModalDelete'
import ModalDetailContact from '../Component/Modal/ModalDetailContact'
import SearchComponent from '../Component/SearchComponent/SearchComponent'
import { DeleteContact, GetContact, GetContactById } from '../redux/Action'
import { useDispatch, useSelector } from 'react-redux'
import Loading from '../Component/Loading/Loading'

const Index = ({
    navigation
}) => {
    const [visible, setVisible] = useState(false)
    const [modal, setModal] = useState(false)
    const [changeText, setChangeText] = useState('')
    const [name, setName] = useState('')
    const [age, setAge] = useState('')
    const [lastName, setLastName] = useState('')
    const [photo, setPhoto] = useState('')
    const [idContact, setIdContact] = useState('')

    const [resultData, setResultData] = useState('')
    const [load, setLoad] = useState(false)
    const dispatch = useDispatch();
    async function getData() {
        setLoad(true)
        await dispatch(GetContact())        
        setLoad(false)
      }
    useEffect(() => {
        getData()
    }, [modal])

    async function getDataById(payload) {
        setLoad(true)
        await dispatch(GetContactById(payload))
        if (state.isLoad == false) {
            setModal(true)
        }
      }
    const state = useSelector(state => state?.fetch);

    console.log( 'ini states',state);
  return (
    <SafeAreaView style={{ backgroundColor: '#252d30', flex: 1 }}>
        <StatusBar backgroundColor={'white'} barStyle={'dark-content'} />
        <Header
            label={'Contact'}
            onAdd={true}
            onPressAdd={() => navigation.navigate('AddContact')}
        />
        <View style={{backgroundColor:'#2791B5', borderTopLeftRadius: responsiveHeight(30), borderTopRightRadius: responsiveHeight(30), height: responsiveHeight(100), flex:1}}>
            <View style={{marginTop: responsiveHeight(20), marginBottom: responsiveHeight(20)}}>
                <SearchComponent 
                    placeholder={'Search Contact'}
                    onChangeText={e => setChangeText(e)}
                />
            </View>
        
            <View style={{ backgroundColor:'#252d30', borderTopLeftRadius: responsiveHeight(30), borderTopRightRadius: responsiveHeight(30), flex:1}}>
                <ScrollView style={{marginTop: responsiveHeight(20),}}>
                    <View>
                        {state.getContact?.map((field, index) => {
                            return (
                                <View key={index}>
                                    <CardListContact
                                    label={field.firstName}
                                    imageCard={field.photo}
                                    onPressDelete={()=> {setVisible(true), setIdContact(field.id)} }
                                    onPressButtonEdit={() => navigation.navigate('EditContact', {
                                        name: field.firstName,
                                        lastNames: field.lastName,
                                        ages: field.age,
                                        photo: field.photo,
                                        id: field.id,
                                        isPhoto: true
                                    })}
                                    // onPressDetail={() => {
                                    //     setModal(true),
                                    //     setName(field.firstName), 
                                    //     setAge(field.age), 
                                    //     setPhoto(field.photo), 
                                    //     setLastName(field.lastName) 
                                    // }}
                                    onPressDetail={() => {
                                        getDataById(field.id)
                                        // setTimeout(() => {
                                            
                                        // }, 3000);
                                    }}
                                    />
                                    <View style={{marginLeft: responsiveHeight(35),backgroundColor:'#3d4142', height: responsiveHeight(1)}}/>
                                </View>
                            )
                        })}
                    </View>
                    
                </ScrollView>
            </View>
        </View>
        <ModalDelete 
            visible={visible} 
            onPress={()=> setVisible(!visible)} 
            setDelete={
                async()=>{
                        dispatch(DeleteContact(idContact))
                        getData()
                        setVisible(!visible)
                }}/>
        <ModalDetailContact 
            visible={modal} 
            onPress={()=> setVisible(!modal)} 
            setVisible={() => setModal(false)}
            firstName={!modal? '': state.getContactById?.firstName}
            lastName={!modal? '': state.getContactById?.lastName}
            age={!modal? '': state.getContactById?.age}
            phoneNumber={'(+62) 8133 0000 000'}
            imgContact={!modal? '': state.getContactById?.photo}
        />
        <Loading isLoading={load}/>
    </SafeAreaView>
  )
}

export default Index

const styles = StyleSheet.create({})